#!/usr/bin/env bash
STARTTIME=$(date +%s)
cd $(dirname $0)
# https://unix.stackexchange.com/questions/148/colorizing-your-terminal-and-shell-environment
red='\033[0;31m'
green='\033[0;32m'
yellow='\e[93m'
no_color='\033[0m'

site_definitions_directory="hgv_data/config/sites"
site_content_directory="hgv_data/sites/"
active_sites=${site_definitions_directory}"/active/"
inactive_sites=${site_definitions_directory}"/inactive/"
starter_theme_name=wpestarter
build_errors=0

# Licenses
wpdbm_license="6059ce4d-04db-48d4-bb84-b188d4211f97"
gf_license="181511784971f1a386c55d94cb42d301"

# http://stackoverflow.com/questions/4023830/how-compare-two-strings-in-dot-separated-version-format-in-bash
vercomp () {
    if [[ $1 == $2 ]]
    then
        return 0
    fi
    local IFS=.
    local i ver1=($1) ver2=($2)
    # fill empty fields in ver1 with zeros
    for ((i=${#ver1[@]}; i<${#ver2[@]}; i++))
    do
        ver1[i]=0
    done
    for ((i=0; i<${#ver1[@]}; i++))
    do
        if [[ -z ${ver2[i]} ]]
        then
            # fill empty fields in ver2 with zeros
            ver2[i]=0
        fi
        if ((10#${ver1[i]} > 10#${ver2[i]}))
        then
            return 1
        fi
        if ((10#${ver1[i]} < 10#${ver2[i]}))
        then
            return 2
        fi
    done
    return 0
}

testvercomp () {
    vercomp $1 $2
    case $? in
        0) op='=';;
        1) op='>';;
        2) op='<';;
    esac
    if [[ $op != $3 ]]
    then
        #echo "Version check ($3) failed: local ($1) $op requirement ($2)"
        return 1
    else
        #echo "Version check passed: local ($1) $op requirement ($2)"
        return 0
    fi
}

# Get the current user's HGV root
function getHgvRoot()
{
    hgv_root=`pwd`
    echo "$hgv_root"
}

function testVagrantSSH()
{
    outputMessage "Checking vagrant status"
    vagrant ssh -c "exit" > /dev/null 2>&1
    if [ "$?" != "1" ]; then
        return 0 # exists
    else
        return 1 # does not exist
    fi
}

function enablePlugins()
{
    if ! isValidInput $1; then
        showNoInputError
    else
        if ! testVagrantSSH; then
            outputMessage "Vagrant not currently running, skipping plugin activation"
        else
            outputMessage "Enabling starter plugins"
            for file in "${active_sites}"*
            do
                dir="/vagrant/${site_content_directory}$1"
                vagrant ssh -c "wp plugin delete hello --path=${dir}"
                vagrant ssh -c "wp plugin activate wp-migrate-db-pro --path=${dir}"
                vagrant ssh -c "wp plugin activate wp-migrate-db-pro-cli --path=${dir}"
                vagrant ssh -c "wp plugin activate wp-migrate-db-pro-media-files --path=${dir}"
                vagrant ssh -c "wp migratedb setting update license ${wpdbm_license} --path=${dir}"
                vagrant ssh -c "wp plugin activate advanced-custom-fields-pro --path=${dir}"

                if checkDir "${site_content_directory}$1/wp-content/plugins/gravityformscli"; then
                    vagrant ssh -c "wp plugin delete gravityforms --path=${dir}"
                    vagrant ssh -c "wp plugin activate gravityformscli --path=${dir}"
                    vagrant ssh -c "wp gf install --key=${gf_license} --activate --path=${dir}"
                fi
            done
            outputMessage "Plugins enabled"
        fi
    fi
}

function updatePlugins()
{
    if ! isValidInput $1; then
        showNoInputError
    else
        if ! testVagrantSSH; then
            outputMessage "Vagrant not currently running, skipping plugin updates"
        else
            outputMessage "Updating plugins"
            for file in "${active_sites}"*
            do
                dir="/vagrant/${site_content_directory}$1"
                vagrant ssh -c "wp plugin update --all --path=${dir}"
            done
            outputMessage "Plugins Updated"
        fi
    fi
}

function getPackageRequirements()
{
    node -pe "require('$(getHgvRoot)/${site_content_directory}$1/package.json').$2" > /dev/null 2>&1

    if [ $? -gt 0 ]; then
        echo "requirement_not_found"
    else
        package_requirement=$(node -pe "require('$(getHgvRoot)/${site_content_directory}$1/package.json').$2")
        echo $package_requirement
    fi
}

function checkLocalNVMVersion()
{
    source ~/.nvm/nvm.sh ~/.bashrc
    nvm current
}

# Usage: testPackageRequirements <project_name> <package.attribute> <local_version>
# Sample: testPackageRequirements wpestarter "engines.node" $(npm --version)
function testPackageRequirements()
{
    requirement=$(getPackageRequirements $1 $2)

    if [ "${requirement}" == "requirement_not_found" ]; then
        return 2
    else
        local=$3

        min_met=$(testvercomp "${local}" "${requirement}" "=")
        min_matched_ok=$?

        min_met=$(testvercomp "${local}" "${requirement}" ">")
        min_met_ok=$?

        if [ "${min_matched_ok}" -eq 0 ] || [ "${min_met_ok}" -eq 0 ]; then
            return 0
        else
            return 1
        fi
    fi
}

function checkNodeModules()
{
    npm outdated --prefix $site_content_directory$1;
}

function checkNPMRequirements()
{
    npm_ok=$(testPackageRequirements $1 "engines.npm" $(npm --version))
    npm_status=$?

    nvm_ok=$(testPackageRequirements $1 "engines.node" $(checkLocalNVMVersion | cut -c 2-))
    nvm_status=$?

    if [ "${npm_status}" -eq 1 ]; then
        npm_notice=${red}"* Your current version of NPM does not meet the package requirements for this project.
  Local: $(npm --version) => Package requirement: $(getPackageRequirements $1 engines.npm)"${no_color}
    fi

    if [ "${nvm_status}" -eq 1 ]; then
        nvm_notice=${red}"\n* Your current version of NVM does not meet the package requirements for this project.
  Local: $(checkLocalNVMVersion | cut -c 2-) => Package requirement: $(getPackageRequirements $1 engines.node)"${no_color}
    fi

    if [[ "${npm_status}" -eq 2 || "${nvm_status}" -eq 2 ]]; then
        outputMessage "The package.json file for your project does not have "engines" correctly specified.  Please update your
  package file with the following:

  "engines" : {
    "npm" : "4.2.0",
    "node" : "7.10.0"
  }," ${yellow}
    elif [[ "${npm_status}" -eq 0 && "${nvm_status}" -eq 0 ]]; then
        outputMessage "Your active versions of NPM and NVM meet the requirements of the current project" ${green}
    else
        outputMessage "${npm_notice}${nvm_notice}"
    fi
}

function checkForRepo()
{
    git ls-remote --quiet --exit-code git@bitbucket.org:libertyconcepts/$1.git

    if [ "$?" == "0" ]; then
        return 0 # exists
    else
        return 1 # does not exist
    fi
}

function cloneRepo()
{
    # Don't bother cloning the repo if a local project folder already exists
    if ! checkDir $site_content_directory$1; then
        outputMessage "Cloning git@bitbucket.org:libertyconcepts/$1.git"
        # If there is not a local project folder and the remote repo exists, clone it
        if checkForRepo $1; then
            git clone git@bitbucket.org:libertyconcepts/$1.git $site_content_directory$1
            git -C $site_content_directory$1 checkout production
        # If there is not a local project folder and the remote repo does not exists, use the '$wpestarter' repo
        else
            outputMessage "Cloning git@bitbucket.org:libertyconcepts/${starter_theme_name}.git"
            git clone git@bitbucket.org:libertyconcepts/wpestarter.git $site_content_directory$1
            outputMessage "updating the remote repo URL to origin git@bitbucket.org:libertyconcepts/$1.git"
            git -C $site_content_directory$1 remote set-url origin git@bitbucket.org:libertyconcepts/$1.git
            git -C $site_content_directory$1 remote -v
            git -C $site_content_directory$1 checkout -b production
        fi
        outputMessage "Repo successfully cloned"
    else
        outputMessage "Local project already exists"
    fi
}

function configureGulpFile()
{
    gulpfile=$(getHgvRoot)/$site_content_directory$1/wp-content/themes/$1/gulpfile.js
    if checkFile $gulpfile; then
        initial_proxy="wpestarter.hgv.test"
        new_proxy="$1.hgv.test"
        outputMessage "Configuring browser-sync proxy in $gulpfile"
        sed -i "" -e "s/$initial_proxy/$new_proxy/g" $gulpfile
        outputMessage "browser-sync proxy configured"
    else
        outputMessage "No gulpfile found"
    fi
}

function configureStarterTheme()
{
    target_dir=$site_content_directory$1/wp-content/themes
    outputMessage "Renaming starter theme"
    mv "${target_dir}/${starter_theme_name}" "$target_dir/$1" > /dev/null 2>&1
    if [ $? == 1 ]; then
        outputMessage "No starter theme found"
    else
        git -C $(getHgvRoot)/$site_content_directory$1 add wp-content/themes/$1
        git -C $(getHgvRoot)/$site_content_directory$1 add wp-content/themes/${starter_theme_name}
        outputMessage "Starter theme renamed to '$1'"
    fi
}

function addGitHooks()
{
    global_git_hooks=$(getHgvRoot)/script/git_hooks

    if ! checkDir $(getHgvRoot)/${site_content_directory}$1/.git/hooks; then
        outputMessage "Hooks directory not found in current project, making it now"
        mkdir $(getHgvRoot)/${site_content_directory}$1/.git/hooks > /dev/null 2>&1
        if [ $? == 1 ]; then
            outputMessage "Error: hooks directory not created"
        else
            outputMessage "${site_content_directory}$1/.git/hooks created"
        fi
    fi

    outputMessage "Adding git hooks from $global_git_hooks"

    for hook in $global_git_hooks/*
    do
        hook_name=${hook##*/}

        if ! checkFile ${site_content_directory}$1/.git/hooks/$hook_name; then
            ln -s $(getHgvRoot)/script/git_hooks/$hook_name ${site_content_directory}$1/.git/hooks/$hook_name
            outputMessage "Symlink created for $hook_name"
        else
            outputMessage "Alias for '$hook_name' already exists, skipping symlink creation"
        fi
    done
}


function removeUnusedGitHooks()
{
    global_git_hooks=$(getHgvRoot)/script/git_hooks
    local_git_hooks=${site_content_directory}$1/.git/hooks

    # compare the global git hooks with the local ones, and remove any that only exist in the local repo
    for hook in $local_git_hooks/*
    do
        hook_name=${hook##*/}

        if ! checkFile $global_git_hooks/$hook_name; then
            rm $local_git_hooks/$hook_name
            outputMessage "Hook removed: $hook_name"
        fi
    done
}

function configureGitHooks()
{
    outputMessage "Configuring git hooks"
    addGitHooks $1
    removeUnusedGitHooks $1
    outputMessage "Git hooks configured"
}

function deleteDirectory()
{
    # Make sure that a site has been specified first
    if isValidInput $1; then
        if checkDir ${site_content_directory}$1; then
            outputMessage "removing directory: ${site_content_directory}$1"
            rm -rf ${site_content_directory}$1
        else 
            outputMessage "directory not found"
        fi
    else 
        outputMessage "no directory specified" ${red}
    fi
}

function disableAllSites()
{
    shopt -s -q dotglob nullglob

    if checkFile ${active_sites}*; then
        mv ${active_sites}* ${inactive_sites}
    fi
}

function updateNodeModules()
{
    # Delete the node_modules directory and install fresh modules.
    rm -r ${site_content_directory}${1}/node_modules > /dev/null 2>&1
    outputMessage "Performing a clean 'npm install'..."
    npm install --prefix ${site_content_directory}${1} > /dev/null 2>&1

    outputMessage "The following modules can be updated in your package.json file.  Any module update should be re-tested
before committing to the repo:"
    checkNodeModules $1
    outputMessage "Module verification complete"
}

function reloadVagrant()
{
    # reload vagrant to force local DNS to update and include the newly enabled site
    outputMessage "reloading vagrant..."
    vagrant reload
    outputMessage "vagrant reloaded"
}

function provisionVagrant()
{
    # reload vagrant to force local DNS to update and include the newly enabled site
    outputMessage "Provisioning new site..."
    vagrant reload --provision
    outputMessage "site provisioned"
}

function verifyWPInstall()
{
    # If Wordpress is already installed, run 'vagrant reload' instead of re-provisioning the site
    docker exec -it $1_web sudo -u www-data wp core is-installed
}

function runWPInstall()
{
    if verifyWPInstall $1; then
        reloadVagrant
    else
        provisionVagrant
    fi
}

# Write the <site>.yml file for a new project
function writeYML()
{
    # the sites directory where the .yml files for each site is stored
    file_name="$1.yml"
    target_file="${active_sites}${file_name}"

    # Make sure the /active sites directory exists
    mkdir -p ${active_sites}

    # set up the structure for the YAML file
        read -d '' yml_file <<EOF
    ---
    wp:
        enviro: $1
        hhvm_domains:
            - $1-hhvm.hgv.test
        php_domains:
            - $1.hgv.test
EOF

    outputMessage "Writing ${file_name}..."
    echo "${yml_file}" > ${target_file}
    # show the user the contents of the .yml file to be written
    echo "${yml_file}"
    outputMessage "${file_name} written"
}

function calculateDuration()
{
    DURATION=$((($(date +%s)-$STARTTIME)/60))
    outputMessage "Command completed in ${DURATION} minutes"
}


# Build a new local site
# usage: $ bash hgv build <project_name> --pull_db=<true/false> --update_plugins=<true/false>
# Note: <remote_domain> and <secret_key> will both be provided by Migrate DB Pro when you copy a site's migration credentials
function build()
{
    UPDATE_PLUGINS=false
    PULL_DB=false

    if ! isValidInput $1; then
        showNoInputError
    else
        site=$1

        for i in "$@"
        do
        case $i in
            --update_plugins=*)
            UPDATE_PLUGINS="${i#*=}"
            shift # past argument=value
            ;;
            --pull_db=*)
            PULL_DB="${i#*=}"
            shift # past argument=value
            ;;
            --fast)
            speed=--fast
            shift # past argument=value
            ;;
            --default)
            DEFAULT=YES
            shift # past argument with no value
            ;;
            *)
                    # unknown option
            ;;
        esac
        done

        if [ ${PULL_DB} = true ]; then
            outputMessage "Please enter the remote domain and secret key for the database you wish to pull, and then hit <enter>" ${green}
            read migration_credentials
        fi

        # disable all other sites before provisioning the new site
        disableAllSites
        writeYML ${site}
        cloneRepo ${site}
        configureProjectFile ${site}
        configureStarterTheme ${site}
        configureGulpFile ${site}
        enable --site=${site} ${speed}
        enablePlugins ${site}

        if [[ "${PULL_DB}" = true ]] && [[ "$migration_credentials" != "" ]]; then
            pullRemoteDatabase ${site} $migration_credentials
        else
            outputMessage "Skipping database migration"
        fi

        if [[ "${UPDATE_PLUGINS}" = true ]]; then
            updatePlugins ${site}
        else
            outputMessage "Skipping plugin updates"
        fi

        calculateDuration
    fi
}

function enable()
{
    # Make sure a site to enable was specified
    for i in "$@"
    do
    case $i in
        --site=*)
        site="${i#*=}"
        shift # past argument=value
        ;;
        --fast)
        fast_enable=true
        shift # past argument=value
        ;;
        --default)
        DEFAULT=YES
        shift # past argument with no value
        ;;
        *)
                # unknown option
        ;;
    esac
    done

    if [ "${site}" == "" ]; then
        outputMessage "No site specified for enabling"  ${red}
        exit 1
    fi

    target_site="${site}.yml"
    target_site_dir=${site_content_directory}${site}

    if ! testVagrantSSH; then
        outputMessage "Running 'vagrant up'..."
        vagrant up
    fi


    # If the target site is in the /inactive sites directory, move the .yml file and reload vagrant
    if checkFile ${inactive_sites}${target_site}; then
        mkdir -p ${active_sites}
        # disable all other sites by moving them from the active folder to the inactive folder
        outputMessage "disabling all sites..."
        disableAllSites
        outputMessage "enabling ${target_site}..."
        mv ${inactive_sites}${target_site} ${active_sites}${target_site}
        runWPInstall ${site}

        if [[ "${fast_enable}" != "true" ]]; then
            configureGitHooks ${site}
            checkNPMRequirements
            updateNodeModules ${site}
        fi

        calculateDuration
        open "http://${site}.hgv.test"
    # If the target site is already in the /active sites directory, make sure Wordpress was fully installed
    elif checkFile ${active_sites}${target_site}; then
        runWPInstall ${site}

        if [[ "${fast_enable}" != "true" ]]; then
            configureGitHooks ${site}
            checkNPMRequirements
            updateNodeModules ${site}
        fi
        calculateDuration
        open "http://${site}.hgv.test"
    else
        outputMessage "no matching site found to enable"  ${red}
    fi
}


# Help docs for this file
if [ "$1" == "-h" ] || [ "$1" == "" ]; then
    printf "\n"
    printf "Usage: hgv_project <command> <site>\n"
    printf "\n"
    printf "    build           create a new <site>.yml file, reload vagrant, and then run vagrant provision\n"
    printf "    enable          move an existing <site>.yml file into the 'active' directory and then run a vagrant reload\n"
    printf "    destroy         completely remove a local site by deleting <site>.yml, $(getHgvRoot)/hgv_data/sites/<site>, and\n"
    printf "                    the SQL database for wpe_<site>\n"
    printf "    enablePlugins   activate and add licenses for commonly used plugins. Does NOT require <site> parameter\n"
    printf "\n"
    exit 1
fi

$@

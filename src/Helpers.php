<?php

namespace Lcwp;

use Composer\Script\Event;
use Dotenv\Dotenv;
use Exception;

class Helpers
{
    public static function outputMessage($message)
    {
        echo "\n===============================\n";
        echo $message;
        echo "\n===============================\n";
    }

    public static function getProjectRoot(Event $event)
    {
        $composer = $event->getComposer();
        return dirname($composer->getConfig()->get('vendor-dir'));
    }

    public static function getProjectName(Event $event)
    {
        self::loadEnv($event);
        return getenv('PROJECT_NAME');
    }

    public static function getThemeName(Event $event)
    {
        self::loadEnv($event);
        return getenv('THEME_NAME');
    }

    public static function loadEnv(Event $event)
    {
        $project_root = Helpers::getProjectRoot($event);
        $dotenv = new Dotenv($project_root);
        $dotenv->load();
    }

    public static function loadEnvLicenses(Event $event)
    {
        try {
            $project_root = Helpers::getProjectRoot($event);

            if (!file_exists($project_root.'/.env.licenses')) {
                throw new Exception('No .env.licenses file found.');
            }

            $dotenv = new Dotenv($project_root, '.env.licenses');
            $dotenv->load();
        } catch (Exception $e) {
            Helpers::outputMessage($e->getMessage());
            exit(1);
        }
    }

    /**
     * Function that prints out a variable and then dies.
     *
     * @param mixed $var      The variable to dump out
     * @param bool  $var_dump Whether to use var_dump instead of default of print_r
     * @param bool  $passthru Whether to die or not
     *
     * @return mixed
     * @access public
     */
    public static function prDie($var, $var_dump = false, $pass_thru = false)
    {
        $from = isset($_SERVER['HTTP_USER_AGENT'])
        && preg_match('/curl/', $_SERVER['HTTP_USER_AGENT']) ? 'cli' : 'http';

        if ($from !== 'cli') {
            echo '<pre>';
        } else {
            echo "\n";
        }

        if (!$var_dump) {
            print_r($var);
        } else {
            var_dump($var);
        }

        if ($from !== 'cli') {
            echo '</pre>';
        } else {
            echo "\n";
            echo "\n";
        }

        if (!$pass_thru) {
            die;
        }
    }


    public static function validateProjectName($project_name)
    {
        try {
            if ($project_name === 'your_project_name') {
                throw new Exception('You have not yet configured the project name in your .env file.');
            }

            return $project_name;
        } catch (Exception $e) {
            Helpers::outputMessage($e->getMessage());
            exit(1);
        }
    }

    public static function validateThemeName($theme_name)
    {
        try {
            if ($theme_name === 'your_theme_name') {
                throw new Exception('You have not yet configured the theme name in your .env file.');
            }

            return $theme_name;
        } catch (Exception $e) {
            Helpers::outputMessage($e->getMessage());
            exit(1);
        }
    }

    public static function validateUserInput($argv)
    {
        try {
            if (!isset($argv[2]) || $argv[2] === '--ansi') {
                throw new Exception('You must specify a project name when running composer setup.  Example: composer setup myproject');
            }
        } catch (Exception $e) {
            Helpers::outputMessage($e->getMessage());
            exit(1);
        }
    }

    public static function wpCliCommand(Event $event)
    {
        $project_name = Helpers::getProjectName($event);
        Helpers::validateProjectName($project_name);
        return "docker exec ${project_name}_web sudo -u www-data wp ";
    }

    public static function checkForRunningContainers(Event $event)
    {
        try {
            $project_name = self::getProjectName($event);
            self::validateProjectName($project_name);

            exec('docker ps --format \'{{.Names}}\'', $container_list, $container_list_exit_code);
            $container_name = $project_name . '_web';

            if (in_array($container_name, $container_list)) {
                Helpers::outputMessage('Container ' . $container_name . ' is already running.');
            } elseif (!empty($container_list)) {
                $container_list_output = "\n";
                foreach ($container_list as $container) {
                    $container_list_output .= '- ' . $container . "\n";
                }

                throw new Exception('You have other running docker containers: ' . $container_list_output . '
To stop all running containers, run "docker stop $(docker ps -aq)"');
            }
        } catch (Exception $e) {
            Helpers::outputMessage($e->getMessage());
            exit(1);
        }
    }
}
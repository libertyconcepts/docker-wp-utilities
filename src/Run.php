<?php

namespace Lcwp;

use Exception;

class Run
{
    public static function containerUp()
    {
        try {
            exec('docker-compose up -d', $output, $exit_code);
            if ($exit_code > 0) {
                throw new Exception('There was a problem running docker-compose up');
            }
        } catch (Exception $e) {
            Helpers::outputMessage($e->getMessage());
            exit(1);
        }
    }
}
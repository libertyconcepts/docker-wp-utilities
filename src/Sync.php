<?php

namespace Lcwp;

use Composer\Script\Event;
use Exception;

class Sync
{
    public static function sync(Event $event, $with_files = false)
    {
        try {
            ManagePlugins::verifyPlugin($event, 'wp-migrate-db-pro-cli');
            ManagePlugins::verifyPlugin($event, 'wp-migrate-db-pro');

            Helpers::loadEnvLicenses($event);
            $remote_url = getenv('PRODUCTION_URL');
            $migration_key = getenv('MIGRATION_KEY');
            ManagePlugins::verifyMigrateDbProLicense($event);

            global $argv;
            foreach($argv as $arg) {
                if ($arg == '--with-files') {
                    $with_files = true;
                }
            }

            $migrate_media_settings = '';
            if ($with_files) {
                ManagePlugins::verifyPlugin($event, 'wp-migrate-db-pro-media-files');
                //--media=<compare|compare-and-remove|remove-and-copy>
                $migrate_media_settings = " --media=remove-and-copy";
            }

            $basic_auth_login = getenv('BASIC_AUTH_LOGIN');
            $basic_auth_pass = getenv('BASIC_AUTH_PASS');
            if (!empty($basic_auth_login) && !empty($basic_auth_pass)) {
                $sync_url = 'https://' . $basic_auth_login . ':' . $basic_auth_pass . '@' . $remote_url;
            } else {
                $sync_url = $remote_url;
            }

            $sync_command = Helpers::wpCliCommand($event)
                ."migratedb pull ".$sync_url." ".$migration_key
                ." --find=".$remote_url
                ." --replace=localhost"
                .$migrate_media_settings;

            Helpers::outputMessage("Syncing with remote database...");
            exec($sync_command, $out, $exit_code);
            if ($exit_code !== 0) {
                throw new Exception("There was an error syncing the local database");
            }
            Helpers::outputMessage("Sync complete");
        } catch (Exception $e) {
            Helpers::outputMessage($e->getMessage());
            exit(1);
        }
    }
}

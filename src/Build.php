<?php

namespace Lcwp;

use Composer\Script\Event;
use Exception;

class Build
{
    public static function build(Event $event)
    {
        try {
            $project_root = Helpers::getProjectRoot($event);

            if (!file_exists($project_root.'/.env')) {
                throw new Exception('No .env file found.  Have you already run composer setup?');
            }

            $project_name = Helpers::getProjectName($event);
            Helpers::validateProjectName($project_name);

            $theme_name = Helpers::getThemeName($event);
            Helpers::validateThemeName($theme_name);

            Helpers::checkForRunningContainers($event);

            Helpers::outputMessage('Building docker images...');
            exec('docker-compose build', $build_output, $build_exit_code);
            if ($build_exit_code > 0) {
                throw new Exception('There was a problem running docker-compose build');
            }

            Run::containerUp($event);

            exec('chown -R '.getenv('USER').': wp-content', $chown_output, $chown_exit_code);
            if ($chown_exit_code > 0) {
                throw new Exception('There was a problem changing ownership of /wp-content');
            }
        } catch (Exception $e) {
            Helpers::outputMessage($e->getMessage());
            exit(1);
        }
    }

    public static function showSummary(Event $event)
    {
        try {
            $project_root = Helpers::getProjectRoot($event);

            if (!file_exists($project_root.'/.env')) {
                throw new Exception('No .env file found.  Have you already run composer setup?');
            }

            $project_name = Helpers::getProjectName($event);
            Helpers::validateProjectName($project_name);

            $theme_name = Helpers::getThemeName($event);
            Helpers::validateThemeName($theme_name);

            $project_summary = "
    # Sequel Pro connection credentials:
    host: 127.0.0.1
    username: root
    password: wordpress
    port: 8081
    
    # Project variables from the .env file
    Project name: ${project_name}
    Theme name: ${theme_name}
            ";

            Helpers::outputMessage("Local project summary:\n".$project_summary);

        } catch (Exception $e) {
            Helpers::outputMessage($e->getMessage());
        }
    }

    public static function installSingleSite(Event $event)
    {
        self::installWordpress($event);
    }

    public static function installMultiSite(Event $event)
    {
        self::installWordpress($event, 'multisite');
    }

    public static function installWordpress(Event $event, $install_type = 'single_site')
    {
        // TODO: Add a check for a --with-plugins flag
        try {
            // TODO: Make sure the wordpress container is running before connecting to it
            Helpers::outputMessage('Installing Wordpress...');

            # Install WP via the wp-cli core install command
            $project_name = Helpers::getProjectName($event);
            Helpers::validateProjectName($project_name);

            // Make sure that the Wordpress core files are ready inside the container
            Helpers::outputMessage('Making sure Wordpress core files exist');
            $ready = false;
            while (!$ready) {
                $check_for_config_file = 'docker exec '.$project_name.'_web php -r \'$config = file_exists("wp-config.php"); if ($config) { exit(0); } else { exit(1); }\'';
                exec($check_for_config_file, $connection_test_output, $connection_test_exit_code);


                if (0 === $connection_test_exit_code) {
                    // TODO: This is hacky, find a better way to ensure WP Core is ready
                    sleep(5);
                    $ready = true;
                } else {
                    echo "...";
                    sleep(3);
                }
            }

            Helpers::outputMessage('Wordpress core files found, moving on.');

            // Make sure sendmail is enabled
            exec('docker exec -i '.$project_name.'_web sendmail -bd');

            // See if Wordpress is already installed
            $check_installation = 'docker exec '.$project_name.'_web sudo -u www-data wp core is-installed';
            exec($check_installation, $install_output, $install_exit_code);

            if ($install_exit_code === 0) {
                throw new Exception("Wordpress is already installed, skipping installation process.");
            } else {
                $cli_install_type = $install_type === 'single_site' ? 'install' : 'multisite-install';
                $multisite_params = $cli_install_type === 'multisite-install' ? '--subdomains' : '';
                $site_url = $install_type === 'single_site' ? 'localhost' : $project_name.'.site';

                $install_wp_command = 'docker exec '.$project_name.'_web sudo -u www-data wp core '.$cli_install_type.' '.$multisite_params.' \
                --url='.$site_url.' \
                --title='.$project_name.' \
                --admin_user=wordpress \
                --admin_password=wordpress \
                --admin_email=noreply@'.$project_name.'.com \
                --skip-email \
                ';

                Helpers::outputMessage('Running: wp core '.$cli_install_type);
                $install_wp = shell_exec($install_wp_command);

                Helpers::outputMessage($install_wp);
            }
        } catch (Exception $e) {
            Helpers::outputMessage($e->getMessage());
        }
    }

    public static function installNodeModules()
    {
        if (!file_exists('node_modules')) {
            Helpers::outputMessage('Installing node modules');
            exec('npm install > /dev/null 2>&1');
        } else {
            Helpers::outputMessage('Updating node modules');
            exec('npm update > /dev/null 2>&1');
        }
    }
}
<?php

namespace Lcwp;

use Composer\Script\Event;
use Exception;

class ManagePlugins
{
    public static function verifyMigrateDbProLicense(Event $event)
    {
        Helpers::loadEnvLicenses($event);
        $wpdbm_license = getenv('WP_DB_MIGATE_PRO_LICENSE');
        exec(Helpers::wpCliCommand($event)."migratedb setting update license ${wpdbm_license}", $out, $exit_code);
        if ($exit_code !== 0) {
            throw new Exception("There was an error trying to activate the Migrate DB Pro license");
        }
    }

    public static function enablePLugin(Event $event, $plugin_name)
    {
        exec(Helpers::wpCliCommand($event)."plugin activate ${plugin_name}", $out, $exit_code);
        return $exit_code;
    }

    public static function verifyPlugin(Event $event, $plugin_name)
    {
        $project_root = Helpers::getProjectRoot($event);

        if (!file_exists($project_root.'/wp-content/plugins/'.$plugin_name)) {
            throw new Exception($plugin_name.' not found, unable to continue with sync.');
        }

        Helpers::outputMessage("Checking status of ".$plugin_name);
        if (self::enablePLugin($event, $plugin_name) !== 0) {
            throw new Exception("There was an error installing ".$plugin_name);
        };
    }
}
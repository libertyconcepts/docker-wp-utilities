<?php
include_once dirname(__FILE__, 2).'/vars.php';

# Install and configure WP plugins for the project
echo "Configuring starter plugins\n";

$docker_command = "sudo -u www-data ";
$wp_cli_dir = "--path=${docker_container_site_path}";

# Delete Hello Dolly
shell_exec($docker_command. "wp plugin delete hello ${wp_cli_dir}");

# Install the Migrate DB Pro plugin suite
$wp_db_migrate_license = getenv('WPDBM_LICENSE');
shell_exec($docker_command. "wp plugin activate wp-migrate-db-pro ${wp_cli_dir}");
shell_exec($docker_command. "wp plugin activate wp-migrate-db-pro-cli ${wp_cli_dir}");
shell_exec($docker_command. "wp plugin activate wp-migrate-db-pro-media-files ${wp_cli_dir}");
shell_exec($docker_command. "wp migratedb setting update license ${wp_db_migrate_license} ${wp_cli_dir}");

#vagrant ssh -c "wp plugin activate advanced-custom-fields-pro ${wp_cli_dir}"

#if checkDir "${site_content_directory}$1/wp-content/plugins/gravityformscli"; then
#    vagrant ssh -c "wp plugin delete gravityforms ${wp_cli_dir}"
#    vagrant ssh -c "wp plugin activate gravityformscli ${wp_cli_dir}"
#    vagrant ssh -c "wp gf install --key=${gf_license} --activate ${wp_cli_dir}"
#fi

#outputMessage "Plugins enabled"

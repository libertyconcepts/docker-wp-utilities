<?php
$parent_dir = realpath(__DIR__ . '/..');
include_once $parent_dir.'/vars.php';
$run_in_docker = 'docker exec -i '.$container_name;

echo "Connecting to container: $container_name\n";

// TODO: run the sendmail start command when the container is initialized, not in this run file.
$start_sendmail = exec($run_in_docker.' sendmail -bd');

// Install WP Core
$install_wp = shell_exec($run_in_docker.' php '.$docker_container_site_path.'/'.$liberty_framework_dir.'/install_core.php');
echo $install_wp;

// TODO: Only run plugin config if not importing a database with Migrate DB Pro
// Install and configure initial plugins
$configure_plugins = shell_exec($run_in_docker.' php '.$docker_container_site_path.'/'.$liberty_framework_dir.'/install/install_plugins.php');
echo $configure_plugins;
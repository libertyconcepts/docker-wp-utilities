<?php

namespace Lcwp;

use Composer\Script\Event;

class Setup {
    public static function checkForRepo(Event $event)
    {
        global $argv;
        Helpers::validateUserInput($argv);
        $repo_found = false;
        Helpers::loadEnv($event);
        $project_name = getenv('PROJECT_NAME');
        $repo_check = shell_exec('git ls-remote --quiet --exit-code git@bitbucket.org:libertyconcepts/'.$project_name.'.git');

        if ($repo_check !== null) {
            $repo_found = true;
        }

        return $repo_found;
    }

    public static function copyDockerComposeFile(Event $event)
    {
        global $argv;
        Helpers::validateUserInput($argv);

        $project_root = Helpers::getProjectRoot($event);

        if (!file_exists($project_root.'/docker-compose.yml')) {
            exec('cp '.__DIR__.'/docker-compose.yml docker-compose.yml');
            Helpers::outputMessage('docker-compose.yml copied to project root');
        } else {
            Helpers::outputMessage('docker-compose.yml already exists, file not copied');
        }
    }

    public static function copyEnvFile(Event $event)
    {
        global $argv;
        Helpers::validateUserInput($argv);
        $project_root = Helpers::getProjectRoot($event);

        if (!file_exists($project_root.'/.env')) {
            exec('cp '.__DIR__.'/config/.env.sample .env');
            Helpers::outputMessage('.env copied to project root');
        } else {
            Helpers::outputMessage('.env already exists, file not copied');
        }
    }

    public static function copyEnvLicensesFile(Event $event)
    {
        global $argv;
        Helpers::validateUserInput($argv);
        $project_root = Helpers::getProjectRoot($event);

        if (!file_exists($project_root.'/.env.licenses')) {
            exec('cp '.__DIR__.'/config/.env.licenses.sample .env.licenses');
            Helpers::outputMessage('.env.licenses copied to project root');
        } else {
            Helpers::outputMessage('.env.licenses already exists, file not copied');
        }
    }

    public static function configEnvFile(Event $event)
    {
        global $argv;
        Helpers::validateUserInput($argv);
        $project_root = Helpers::getProjectRoot($event);
        exec('sed -i "" -e "s:your_project_name:'.$argv[2].':g" '.$project_root.'/.env');
        exec('sed -i "" -e "s:your_theme_name:'.$argv[2].':g" '.$project_root.'/.env');
        Helpers::outputMessage(file_get_contents($project_root.'/.env'));
    }
}



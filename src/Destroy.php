<?php

namespace Lcwp;

use Exception;

class Destroy
{
    public static function destroy()
    {
        try {
            $skull = file_get_contents(__DIR__.'/ascii_skull.txt');
            $warning_message = <<< EOF
    Whoa there! You're about to do something drastic.
    Running this command will destroy your local site, and remove the local database.
    Are you sure you want to proceed?
    Type "proceed" to continue with site deletion: 
EOF;

            Helpers::outputMessage("\n".$skull."\n\n".$warning_message."\n");
            $confirmation = rtrim(fgets(STDIN), "\n\r");

            if ($confirmation !== 'proceed') {
                throw new Exception('Abort codes accepted, missiles standing down.');
            }

            exec('docker-compose down');
        } catch (Exception $e) {
            Helpers::outputMessage($e->getMessage());
        }
    }
}